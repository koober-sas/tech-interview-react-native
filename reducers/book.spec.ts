import * as Book from './book';

describe('Reducer', () => {
  test('ADD_BOOK', () => {
    const state = {
      ...Book.initialState,
    };
    const expectedState = {
      ...Book.initialState,
      index: {
        ...Book.initialState.index,
        '2': {
          pages: ['a', 'z', 'e'],
          id: '2',
          title: 'Title',
        },
      },
    };
    expect(
      Book.reducer(state, Book.addBook('2', 'Title', ['a', 'z', 'e'])),
    ).toEqual(expectedState);
  });
});
