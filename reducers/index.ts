import {combineReducers} from 'redux';
import * as Book from './book';

export type StateType = {
  book: Book.StateType;
};

export {Book};

export const reducer = combineReducers({
  book: Book.reducer,
});
