export type StateType = {
  index: {
    [key: string]: Book;
  };
};

export type Book = {
  pages: string[];
  title: string;
  id: string;
  validated?: boolean;
};

export const initialState: StateType = {
  index: {},
};

type actionType = {
  type: string;
  payload: any;
};

export const addBook = (
  bookId: string,
  title: string,
  pages: Array<string>,
) => {
  return {
    type: 'ADD_BOOK',
    payload: {
      id: bookId,
      pages,
      title,
    },
  };
};

export const reducer = (state = initialState, action: actionType) => {
  switch (action.type) {
    case 'ADD_BOOK': {
      const {index} = state;
      const {id, pages, title} = action.payload;
      return {index: {...index, [id]: {id, pages, title, validated: false}}};
    }
    default:
      return state;
  }
};
