import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import {reducer} from './reducers/index';
import {Level1LibraryScreen} from './component/level1libraryscreen';

const store = createStore(reducer);

const App = () => {
  return (
    <Provider store={store}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{flex: 1}}>
        <Level1LibraryScreen />
      </SafeAreaView>
    </Provider>
  );
};

export default App;
