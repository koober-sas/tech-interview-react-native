import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';

class Component extends React.Component {
  static displayName = 'Level2BookScreen';
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text>ID</Text>
        <Text>Title</Text>
        <View style={{borderWidth: 1, padding: 8, marginBottom: 8}}>
          <Text style={{alignSelf: 'center'}}>Page 1</Text>
          <Text>
            Blablabla Blablabla Blablabla Blablabla Blablabla Blablabla
            Blablabla Blablabla Blablabla Blablabla Blablabla Blablabla
            Blablabla Blablabla Blablabla
          </Text>
        </View>
        <View style={{borderWidth: 1, padding: 8}}>
          <Text style={{alignSelf: 'center'}}>Page 2</Text>
          <Text>
            Blablabla Blablabla Blablabla Blablabla Blablabla Blablabla
            Blablabla Blablabla Blablabla Blablabla Blablabla Blablabla
            Blablabla Blablabla Blablabla
          </Text>
        </View>
        <View style={{flexDirection: 'row', paddingTop: 12}}>
          <TouchableOpacity
            style={{
              margin: 8,
              justifyContent: 'center',
              alignItems: 'center',
              height: 50,
              width: 100,
              backgroundColor: 'green',
            }}>
            <Text>Validate</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              margin: 8,
              justifyContent: 'center',
              alignItems: 'center',
              height: 50,
              width: 100,
              backgroundColor: 'red',
            }}>
            <Text>Refuse</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export {Component as Level2BookScreen};
