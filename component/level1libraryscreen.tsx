import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import * as Store from '../reducers';
import {fetchBook} from '../data/book';

type Props = {};

type State = {
  books: {
    [key: string]: Store.Book.Book;
  };
};

class Component extends React.Component<Props, State> {
  static displayName = 'Level1LibraryScreen';

  state: State = {
    books: {},
  };

  async componentDidMount() {
    const books = await fetchBook();
    this.setState({books});
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        {Object.values(this.state.books).map((book) => (
          <TouchableOpacity
            key={book.id}
            style={{
              borderRadius: 8,
              padding: 8,
              margin: 8,
              backgroundColor: '#E8F8F5',
            }}>
            <Text>Id: {book.id}</Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  }
}

export {Component as Level1LibraryScreen};
