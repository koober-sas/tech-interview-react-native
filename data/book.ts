// NO CHANGE ALLOWED
import {Book} from '../reducers';

export const fetchBook = async (): Promise<{
  [key: string]: Book.Book;
}> => {
  return later(3000, {
    '1': {
      pages: ['where', 'is', 'jim', 'where', 'is', 'jim', 'where', 'is', 'jim'],
      id: '1',
      title: 'Title of the book1',
    },
    '2': {
      pages: ['where', 'is', 'jim', 'where', 'is', 'jim'],
      id: '2',
      title: 'Title of the book2',
    },
    '3': {
      pages: ['where', 'is', 'jim'],
      id: '3',
      title: 'Title of the book3',
    },
    '4': {
      pages: ['where', 'is', 'jim'],
      id: '4',
      title: 'Title of the book4',
    },
    '5': {
      pages: ['where', 'is', 'jim'],
      id: '5',
      title: 'Title of the book5',
    },
    '6': {
      pages: ['where', 'is', 'jim'],
      id: '6',
      title: 'Title of the book6',
    },
    '7': {
      pages: ['where', 'is', 'jim'],
      id: '7',
      title: 'Title of the book7',
    },
    '8': {
      pages: ['where', 'is', 'jim'],
      id: '8',
      title: 'Title of the book8',
    },
    '9': {
      pages: ['where', 'is', 'jim'],
      id: '9',
      title: 'Title of the book9',
    },
    '11': {
      pages: ['where', 'is', 'jim', 'where', 'is', 'jim', 'where', 'is', 'jim'],
      id: '11',
      title: 'Title of the book11',
    },
    '12': {
      pages: ['where', 'is', 'jim', 'where', 'is', 'jim'],
      id: '12',
      title: 'Title of the book12',
    },
    '13': {
      pages: ['where', 'is', 'jim'],
      id: '13',
      title: 'Title of the book13',
    },
    '14': {
      pages: ['where', 'is', 'jim'],
      id: '14',
      title: 'Title of the book14',
    },
    '15': {
      pages: ['where', 'is', 'jim'],
      id: '15',
      title: 'Title of the book15',
    },
    '16': {
      pages: ['where', 'is', 'jim'],
      id: '16',
      title: 'Title of the book16',
    },
    '17': {
      pages: ['where', 'is', 'jim'],
      id: '17',
      title: 'Title of the book17',
    },
    '18': {
      pages: ['where', 'is', 'jim'],
      id: '18',
      title: 'Title of the book18',
    },
    '19': {
      pages: ['where', 'is', 'jim'],
      id: '19',
      title: 'Title of the book19',
    },
  } as {[key: string]: Book.Book});
};

export const later = (delay: number, value: any): Promise<any> => {
  return new Promise(function (resolve) {
    setTimeout(resolve, delay, value);
  });
};
