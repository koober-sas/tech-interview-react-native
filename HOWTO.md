# Tech Interview

## Intention

This test evaluates candidates's skills specifically on these specific domains :
- React
- Redux
- React Native
- Typescript 
- Functional Programming

Any "extra" showing that you are mastering the subject could be added and will be taken in account.


## Guidelines

- [duplicate](https://help.github.com/articles/duplicating-a-repository/) this repository (do **not** fork it)
- solve the levels in ascending order
- commit often to show the evolution of the code (prefer atomic commits)

## What we expect

- code should be clean (Latest ECMA syntax, Typescript is mandatory)
- tests should be present (Show your best skills : TDD, unit, integration and more)
- comments could be added when you need to clarify a design decision or assumptions about the spec
- documentation could be added to clarify any details (how to run tests, etc)
- redux should be present with all features

This application should be able to display a collection of books, see the content of each book and be able to validate or refuse a specific book.

## Level 1

In this level, you must list all the books using redux instead of using the state.

The `Level1LibraryScreen` should :

- Display book's information (title, number of pages, is validated)
- Move data from the state to redux

## Level 2

In this level, you must be able to access to one book, see the content and validate or not the book.

The `Level2BookScreen` should :

- Be able to access to one selected book
- Display the content of the book (page, id, title)
- Validate book when clicking on the corresponding button
- Refuse book when clicking on the corresponding button
- Every data must be stored in redux

## Level 3 (Bonus Expert)

Assume now that the screens of this test are in a big project, with complex nested navigation and huge set of data. Try to refactor pieces of code (written by us, written by you) that could cause a performance issue and explain the reason of your design in a code comment.


